import 'package:flutter/material.dart';

import 'models/user_model.dart';
import 'package:http/http.dart' as http;

class PostDataApi extends StatefulWidget {
  const PostDataApi({Key? key}) : super(key: key);

  @override
  _PostDataApiState createState() => _PostDataApiState();
}

class _PostDataApiState extends State<PostDataApi> {
  UserModel? _user;

  final TextEditingController titleControler = TextEditingController();
  final TextEditingController valueControler = TextEditingController();

  void _submitted(BuildContext context) async {
    final String title = titleControler.text;
    final String value = valueControler.text;
    // final String title = "hello";
    // final String value = "Merdeka";
    final UserModel? user = await createUser(title, value);
    setState(() {
      _user = user;
    });
    Navigator.pop(context);
  }

  //func createUser
  Future<UserModel?> createUser(String title, String value) async {
    try {
      Dialogs.showLoadingDialog((context), _keyLoader); //invoking login
      //adress api
      var apiUrl =
          Uri.parse("https://achmadhilmy-sanbercode.my.id/api/v1/news");
      final res =
          await http.post(apiUrl, body: {"title": title, "value": value});
      if (res.statusCode == 201) {
        final String resString = res.body;
        return userModelFromJson(resString);
      }
      Navigator.of(context).pop(context);
    } catch (err) {
      print(err.toString());
    }
  }

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Post Data'),
      ),
      body: Container(
          padding: EdgeInsets.all(32),
          child: Column(children: [
            TextField(
              controller: titleControler,
              decoration: InputDecoration(
                  border: OutlineInputBorder(), labelText: 'title'),
            ),
            SizedBox(height: 10),
            TextField(
              controller: valueControler,
              decoration: InputDecoration(
                  border: OutlineInputBorder(), labelText: 'Value'),
              maxLines: 5,
            ),
            SizedBox(height: 10),
            _user == null
                ? Container()
                : Text(
                    "the user ${_user!.title} is created and  id ${_user!.id}  ")
          ])),
      floatingActionButton: FloatingActionButton(
          onPressed: () => _submitted(context),
          tooltip: 'Increment',
          child: Icon(Icons.add)),
    );
  }
}

//class Dialogs
class Dialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              child: SimpleDialog(
                backgroundColor: Colors.black54,
                children: [
                  CircularProgressIndicator(),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Please Wait ....",
                    style: TextStyle(color: Colors.blueAccent),
                  )
                ],
              ),
              onWillPop: () async => false);
        });
  }
}
