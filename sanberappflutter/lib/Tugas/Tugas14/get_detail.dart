import 'package:flutter/material.dart';

// ignore: camel_case_types, must_be_immutable
class getDetailScreen extends StatefulWidget {
  List? value;
  getDetailScreen({Key? key, required this.value}) : super(key: key);

  @override
  _getDetailScreenState createState() => _getDetailScreenState(value);
}

// ignore: camel_case_types
class _getDetailScreenState extends State<getDetailScreen> {
  List? value;
  _getDetailScreenState(this.value);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('detail screen news'),
        ),
        body: Container(
            padding: EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(value![0],
                    style:
                        TextStyle(fontSize: 16, fontWeight: FontWeight.w700)),
                SizedBox(
                  height: 12,
                ),
                Text(value![1],
                    style:
                        TextStyle(fontSize: 16, fontWeight: FontWeight.w400)),
              ],
            )));
  }
}
