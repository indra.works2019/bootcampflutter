import 'dart:convert';


//post
UserModel userModelFromJson(String str) { 
  return UserModel.fromJson(json.decode(str));
} 
//get


class UserModel {
 
  String id;
  String title;
  String value;
  DateTime createdAt;
  
 //constructor
  UserModel({
    required this.id,
    required this.title,
    required this.value,
    required this.createdAt,
   
  });
  //data json ddari servver map  kesini 
  //constructor factory yg memaping instance obj dari-ke-json
  factory UserModel.fromJson(Map<String,dynamic> json)=> UserModel(
    id:json['id'],
    title:json['title'],
    value:json['value'],
    createdAt:DateTime.parse(json['createdAt'])
  );
  //data client ke server
  Map<String,dynamic> toJson()=> {
    "title":title,
    "value":value,
    "id":id,
    "createdAt":createdAt.toIso8601String(),
  };

}


