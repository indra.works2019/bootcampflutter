import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sanberappflutter/Tugas/Tugas14/get_detail.dart';
import 'package:sanberappflutter/Tugas/Tugas14/postData.dart';

class GetDataScreen extends StatefulWidget {
  const GetDataScreen({Key? key}) : super(key: key);

  @override
  _GetDataScreenState createState() => _GetDataScreenState();
}

class _GetDataScreenState extends State<GetDataScreen> {
  final String url = "https://achmadhilmy-sanbercode.my.id/api/v1/news";
  List? data;
  @override
  void initState() {
    _getRefreshData();
    super.initState();
  }

  Future<void> _getRefreshData() async {
    this.getJsonData(context);
  }

  Future<String?> getJsonData(BuildContext context) async {
    var res =
        await http.get(Uri.parse(url), headers: {"Accept": "application/json"});
    print(res.body);
    //hasil data res.body
    // {"status":200,"message":"Success","data":[{"id":231,"title":"Data Terbaik",
    // "value":"Perubahan",
    // "created_at":"2021-09-02T19:38:19.000000Z","updated_at":"2021-09-03T00:15:39.000000Z"}
    setState(() {
      var convertDataToJson = jsonDecode(res.body);
      data = convertDataToJson['data'];//yg diambil field data saja 
      print("hasil data:$data");
    });
    //hasil data:[{id: 262, title: Header, value: Head adalah Kepala, 
    //Header adalah yang mengepalai, created_at: 2021-09-03T02:36:10.000000Z, 
    //updated_at: 2021-09-03T02:36:10.000000Z},
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Get Data Api'),
      ),
      body: RefreshIndicator(
        onRefresh: _getRefreshData,
        child: ListView.builder(
            itemCount: data == null ? 0 : data!.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                  margin: EdgeInsets.all(5.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      GestureDetector(
                        //ktika diclik pada list maka akan re-route kehalaman baru
                        //navigator push bawa value title dan value 
                          onTap: () {
                            var data1 = data![index]['title'];
                            var data2 = data![index]['value'];
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => getDetailScreen(
                                        value: [data1, data2])));
                          },
                          child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        data![index]['title']!,
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w700),
                                      ),
                                      Icon(Icons.chevron_right),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 3,
                                  ),
                                  Divider(
                                      height: 20,
                                      thickness: 1,
                                      endIndent: 25,
                                      indent: 15)
                                ],
                              )))
                    ],
                  ));
            }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => PostDataApi()));
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
