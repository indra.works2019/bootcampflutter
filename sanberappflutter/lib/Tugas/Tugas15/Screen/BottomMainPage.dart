import 'package:flutter/material.dart';
import 'package:sanberappflutter/Tugas/Tugas15/Screen/LoginScreen.dart';
import 'package:sanberappflutter/Tugas/Tugas15/Screen/Telegram.dart';
import 'package:sanberappflutter/Tugas/Tugas15/Screen/Contact.dart';
import 'package:sanberappflutter/Tugas/Tugas15/Screen/HomeScreen.dart';

class BottomMainPage extends StatefulWidget {
  const BottomMainPage({Key? key}) : super(key: key);

  @override
  _BottomMainPageState createState() => _BottomMainPageState();
}

class _BottomMainPageState extends State<BottomMainPage> {
  final pages = [
    HomeScreen(),
    LoginScreen(),
    Telegram(),
    Contact(),
  ];
  int selectedIndex = 0;
  void onTap(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //bottom navigation bar ada di class scafold
        bottomNavigationBar: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
            BottomNavigationBarItem(icon: Icon(Icons.login), label: 'Login'),
            BottomNavigationBarItem(icon: Icon(Icons.chat), label: 'chat'),
            BottomNavigationBarItem(icon: Icon(Icons.phone), label: 'contact'),
          ],
          currentIndex: 0,
          type: BottomNavigationBarType.fixed,
          fixedColor: Colors.blueAccent,
          onTap: onTap,
        ),
        body: pages.elementAt(selectedIndex));
  }
}

/*
Kesimpulan route bisa di buat dengan routing management 
yang diinisialisasi dari main page ,dengan proptries naviaiton pushedNmaed /pop
bisa jug amelalui drawer dan bottomnavigationbar class scafold
utk index ditulis di bagian body 



*/