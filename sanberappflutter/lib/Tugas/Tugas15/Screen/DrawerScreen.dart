import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class DrawerScreen extends StatefulWidget {
  //const DrawerScreen({Key? key}) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}



class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
      children: [
        UserAccountsDrawerHeader(
          accountName: Text('Indra Suryawan'),
          currentAccountPicture: CircleAvatar(
            backgroundImage: AssetImage("assets/img/indra_surya.jpg"),
          ),
          accountEmail: Text('indra.works2019@gmail.com'),
        ),
      ],
    ));
  }
}

class DrawerListTile extends StatelessWidget {
   final IconData? iconData;
  final String? title;
  final VoidCallback? onTilePressed;
  //construcornya
  const DrawerListTile({Key? key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
        onTap: onTilePressed,
        dense: true,
        leading: Icon(iconData),
        title: Text(title!, style: TextStyle(fontSize: 16)));
  }
}
