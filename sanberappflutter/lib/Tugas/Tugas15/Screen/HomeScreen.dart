import 'package:flutter/material.dart';
import 'package:sanberappflutter/Tugas/Tugas12/DrawerScreen.dart';

//BotomNavigation harus dibangun di stfullwidget krn harus ada state utk pindahpage
class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  void searchOperation(String value) {}
  @override
  Widget build(BuildContext context) {
    List<String> cities = [
      'Berlin',
      'Roma',
      'Tokyo',
      'Monas',
      'Paris',
      'London'
    ];
    return Scaffold(
      appBar: AppBar(
        title: Text('HomeScreen'),
      ),
      drawer: DrawerScreen(),
      body: SafeArea(
          child: Column(children: [
        Container(
          margin: EdgeInsets.only(top: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Image.asset(
                'assets/image/add_shopping_cart.png',
                width: 25,
                height: 25,
              ),
              SizedBox(
                width: 10,
              ),
              Image.asset(
                'assets/image/notifications.png',
                width: 25,
                height: 25,
              ),
              SizedBox(
                width: 10,
              ),
            ],
          ),
        ),
        Container(
            margin: EdgeInsets.only(left: 15, top: 10),
            child: Row(
              children: [
                Text('Welcome,',
                    style: TextStyle(color: Colors.blue, fontSize: 45)),
              ],
            )),
        Container(
            margin: EdgeInsets.only(left: 15),
            child: Row(
              children: [
                Text('Hilmy,',
                    style: TextStyle(color: Colors.black, fontSize: 35)),
              ],
            )),
        Container(
            margin: EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 10),
            child: TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  prefixIcon: Icon(Icons.search, color: Colors.blue),
                  hintText: "Search...",
                  hintStyle: TextStyle(color: Colors.blue)),
              onChanged: searchOperation,
            )),
        Container(
          margin: EdgeInsets.only(left: 2),
          child: Text(
            'Recomended Place',
            style: TextStyle(color: Colors.black, fontSize: 25),
          ),
        ),
        //GridViewBuilder
        GridView.builder(
            physics: ScrollPhysics(),
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: MediaQuery.of(context).size.width /
                  (MediaQuery.of(context).size.height / 4.5),
              mainAxisSpacing: 2.0,
              crossAxisSpacing: 2.0,
            ),
            itemCount: 6,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                child: Card(
                  child: Center(
                    child:
                        Image.asset('assets/image/' + cities[index] + '.png'),
                  ),
                ),
              );
            })
      ])),
    );
  }
}







  //const HomeScreen({ Key key }) : super(key: key);
  
 

/*
catatan:
SafeArea:https://stackoverflow.com/questions/49227667/using-safearea-in-flutter
List.generate(namePlaces.length,index){
                        return GridTile( 
                          child:GestureDetector( 
                            child:Container( 
                              decoration:BoxDecoration(  
                                image:DecorationImage( 
                                  fit:BoxConstraintsTween,
                                  image:AssetImage('assets/image/'+namePlaces+"png")
                                )
                              )
                            )
                          )
                        );
                     }
   child:  Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [Icon(Icons.ac_unit), Icon(Icons.ac_unit)],
      ),
      Text(
        "Welcome",
        style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
      ),
      Text(
        "Hilmy",
        style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
      ),
      TextField(
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            prefixIcon: Icon(Icons.search, color: Colors.blue),
            hintText: "Search...",
            hintStyle: TextStyle(color: Colors.grey)),
        onChanged: searchOperation,
      ),

      ),
     

*/
