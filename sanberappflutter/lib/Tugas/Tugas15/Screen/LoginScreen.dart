import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  //const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      margin: EdgeInsets.all(15),
      child: Column(
        children: [
          Container(
              margin: EdgeInsets.only(top: 20, bottom: 10),
              child: Text('Sanber Flutter',
                  style: TextStyle(fontSize: 35, color: Colors.blue))),
          Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              child: Image.asset(
                'assets/image/flutter.png',
                width: 100,
                height: 100,
              )),
          Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    //prefixIcon: Icon(Icons.search, color: Colors.blue),
                    hintText: "Name",
                    hintStyle: TextStyle(color: Colors.blue)),
              )),
          Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    //prefixIcon: Icon(Icons.search, color: Colors.blue),
                    hintText: "Password",
                    hintStyle: TextStyle(color: Colors.blue)),
              )),
          Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              child: Text('Forget Password',
                  style: TextStyle(fontSize: 15, color: Colors.blue))),
          Container(
              margin: EdgeInsets.only(top: 25, bottom: 10),
              child: SizedBox(
                  width: double.infinity,
                  child: RaisedButton(
                    color: Colors.blue,
                    child: Text("Login",
                        style: TextStyle(fontSize: 18, color: Colors.white)),
                    onPressed: () {},
                  ))),
          Container(
              margin: EdgeInsets.only(
                top: 12,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text('Dont have account ?',
                      style: TextStyle(color: Colors.black)),
                  Text('Subscribe', style: TextStyle(color: Colors.blue))
                ],
              )),
        ],
      ),
    ));
  }
}
