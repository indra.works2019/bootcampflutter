import 'package:flutter/material.dart';
// import 'package:sanberappflutter/Tugas/Tugas14/get_data.dart';
// import 'package:sanberappflutter/Tugas/Tugas14/postData.dart';
// import 'package:sanberappflutter/Tugas/Tugas12/Telegram.dart';
// import 'package:sanberappflutter/Tugas/Tugas13/HomeScreen.dart';
// import 'package:sanberappflutter/Tugas/Tugas13/LoginScreen.dart';
import 'package:sanberappflutter/Tugas/Tugas15/Screen/BottomMainPage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          //visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        // home: Telegram()  //tugas 12,
        // home: PostDataApi() //tugas 13 a,
        home: BottomMainPage() //tugas 14 ,
        // home: LoginScreen() //tugas 13 b,
        );
  }
}
//https://oflutter.com/organized-navigation-named-route-in-flutter/