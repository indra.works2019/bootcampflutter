// ignore_for_file: file_names, use_key_in_widget_constructors

import 'package:flutter/material.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawwerScreenState createState() => _DrawwerScreenState();
}

class _DrawwerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
      children: <Widget>[
        // UserAccountsDrawerHeader(
        //     //<!--  //? #Bonus (10 poin) -- DrawerScreen.dart --
        //     //? agar ubahlah gambar pada drawerScreen.dart menjadi individu masing masing, untuk nama dan email juga beri yang dari email yang terdaftar di sanbercode.com -->
        //     //1.Tuliskan coding disini
        //     //  account nama
        //     // end coding
        //     //
        //     //2.Tuliskan coding disini
        //     //   gambar masing masing peserta
        //     // end coding
        //     //
        //     //3.Tuliskan coding disini
        //     //   account email
        //     // end coding

        //     ),

         
        UserAccountsDrawerHeader(
          accountName: Text('Indra Suryawan'),
          currentAccountPicture: CircleAvatar(
            backgroundImage: AssetImage("assets/img/indra_surya.jpg"),
          ),
          accountEmail: Text('indra.works2019@gmail.com'),
        ),
      



        DrawerListTile(
          iconData: Icons.group,
          title: "NewGroup",
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.lock,
          title: "New Secret Group",
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.notifications,
          title: "New Channel Chat",
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.contacts,
          title: "contacts",
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.bookmark_border,
          title: "Saved Message",
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.phone,
          title: "Calls",
          onTilePressed: () {},
        )
      ],
    ));
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;

  const DrawerListTile({Key? key, required this.iconData, required this.title, required this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
