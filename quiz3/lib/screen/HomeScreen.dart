// ignore_for_file: file_names, use_key_in_widget_constructors, must_be_immutable, prefer_const_constructors, avoid_unnecessary_containers

import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  List<String> countries = [
    "Tokyo",
    "Berlin",
    "Roma",
    "Monas",
    "London",
    "Paris"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: Padding(
      padding: const EdgeInsets.all(5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              IconButton(
                  icon: const Icon(Icons.notifications), onPressed: () {}),
              IconButton(icon: const Icon(Icons.extension), onPressed: () {})
            ],
          ),
          const SizedBox(height: 15),
          Text.rich(
            TextSpan(
              children: <TextSpan>[
                TextSpan(
                  text: "Welcome, \n",
                  style: TextStyle(color: Colors.blue[300]),
                ),
                TextSpan(
                  text: "Hilmy, \n",
                  style: TextStyle(color: Colors.blue[900]),
                ),
              ],
            ),
            style: const TextStyle(fontSize: 30),
          ),
          const SizedBox(height: 10),
          TextField(
            decoration: InputDecoration(
                prefixIcon: const Icon(Icons.search, size: 18),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                hintText: "Search"),
          ),
          const SizedBox(height: 5),
          const Text(
            "Recomended Place",
            style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
          ),
          // <!-- //? #Soal No 2 (20 poin) -- HomeScreen.js -- Function HomeScreen
          //? Buatlah 1 komponen GridView dengan input berasal dari assets/img yang sudah disediakan

          //? dan memiliki 2 kolom, sehingga menampilkan 2 item per baris (horizontal)
          //? untuk tampilan apabila ada warning boleh diabaikan asal data gambar tampil
          // -->
          // tuliskan coding disini
          GridView.builder(
              physics: ScrollPhysics(),
              shrinkWrap: true,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: MediaQuery.of(context).size.width /
                    (MediaQuery.of(context).size.height / 4),
                mainAxisSpacing: 2.0,
                crossAxisSpacing: 2.0,
              ),
              itemCount: 6,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  child: Card(
                    child: Center(
                      child: Image.asset(
                          'assets/img/' + countries[index] + '.png'),
                    ),
                  ),
                );
              })

          // // end coding
        ],
      ),
    )));
  }
}




//final countries = ["Tokyo", "Berlin", "Roma", "Monas", "London", "Paris"];
/*
      

               return  Container(
                  padding: const EdgeInsets.all(8),
                  child: Image.asset("assets/image/"+ countries[index]+".png"),
                  color: Colors.teal[100],
                );
*/

/*
Padding(
        padding: const EdgeInsets.all(5.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                    icon: const Icon(Icons.notifications), onPressed: () {}),
                IconButton(icon: const Icon(Icons.extension), onPressed: () {})
              ],
            ),
            const SizedBox(height: 20),
            Text.rich(
              TextSpan(
                children: <TextSpan>[
                  TextSpan(
                    text: "Welcome, \n",
                    style: TextStyle(color: Colors.blue[300]),
                  ),
                  TextSpan(
                    text: "Hilmy, \n",
                    style: TextStyle(color: Colors.blue[900]),
                  ),
                ],
              ),
              style: const TextStyle(fontSize: 30),
            ),
            const SizedBox(height: 10),
            TextField(
              decoration: InputDecoration(
                  prefixIcon: const Icon(Icons.search, size: 18),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10)),
                  hintText: "Search"),
            ),
            const SizedBox(height: 10),
            const Text(
              "Recomended Place",
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
            ),
            // <!-- //? #Soal No 2 (20 poin) -- HomeScreen.js -- Function HomeScreen
            //? Buatlah 1 komponen GridView dengan input berasal dari assets/img yang sudah disediakan

            //? dan memiliki 2 kolom, sehingga menampilkan 2 item per baris (horizontal)
            //? untuk tampilan apabila ada warning boleh diabaikan asal data gambar tampil
            // -->
            // tuliskan coding disini
            // Image.asset('assets/img/Tokyo.png')
            GridView.builder(
              itemCount: countries.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount:
                    MediaQuery.of(context).orientation == Orientation.landscape
                        ? 3
                        : 2,
                crossAxisSpacing: 8,
                mainAxisSpacing: 8,
                childAspectRatio: (2 / 1),
              ),
              itemBuilder: (
                context,
                index,
              ) {
                return GestureDetector(
                  onTap: () {
                    //no code
                  },
                  child: Container(
                    color: RandomColorModel().getColor(),
                    // child:
                    //     Image.asset("assets/img/" + countries[index] + ".png")
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                                "assets/img/" + countries[index] + ".png"))),
                  ),
                );
              },
            )

            // // end coding
          ],
        ),
      ),

https://stackoverflow.com/questions/62666204/how-to-calculate-childaspectratio-for-gridview-builder-in-flutter

*/