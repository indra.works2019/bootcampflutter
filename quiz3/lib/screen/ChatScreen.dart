// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:quiz3/models/Chart_model.dart';

import 'DrawerScreen.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Chat Screen"),
        actions: const <Widget>[
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(Icons.search),
          )
        ],
      ),
      drawer: DrawerScreen(),
      // <!--  //? #Soal No 3 (15 poin) -- ChatScreen.dart --
      //? Buatlah ListView widget agar dapat menampilkan list title, subtitle dan trailling, dan styling agar dapat
      //tampil baik di device -->
      //Tuliskan coding disini//
      body: ListView.separated(
        itemBuilder: (ctx, i) {
          return ListTile(
            leading: CircleAvatar(
              radius: 28,
              backgroundImage: NetworkImage(items[i].profileUrl!),
            ),
            title: Text(
              items[i].name!,
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
            subtitle: Text(items[i].message!),
            trailing: Text(items[i].time!),
          );
        },
        separatorBuilder: (ctx, i) {
          return const Divider();
        },
        itemCount: items.length,
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.create, color: Colors.white),
        backgroundColor: const Color(0xFF65a9e0),
        onPressed: () {},
      ),
    );

    //end coding
  }
}
