// ignore_for_file: file_names, use_key_in_widget_constructors, non_constant_identifier_names, implementation_imports, prefer_const_constructors, avoid_unnecessary_containers

import 'package:flutter/material.dart';

class GridViewPage extends StatelessWidget {
  // const GridViewPage({ Key? key }) : super(key: key);
  List<String> countries = [
    'Tokyo',
    'Berlin',
    'Monas',
    'Roma',
    'Paris',
    'London'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Title")),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            ListView.builder(
              shrinkWrap: true,
              physics: ScrollPhysics(),
              itemBuilder: (context, position) {
                return ListTile(
                  title: Text('List Item'),
                );
              },
              itemCount: 20,
            ),
            GridView.builder(
                physics: ScrollPhysics(),
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: (3 / 2),
                ),
                itemCount: 6,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    child: Card(
                      child: Center(
                        child: Image.asset(
                            'assets/img/' + countries[index] + '.png'),
                      ),
                    ),
                  );
                }),
          ],
        ),
      ),
    );
  }
}
//   Widget buildImageCard(int index) => Card(
//         margin: EdgeInsets.zero,
//         shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.circular(8),
//         ),
//         child: Container(
//           margin: EdgeInsets.all(8),
//           child: ClipRRect(
//             borderRadius: BorderRadius.circular(8),
//             child: Image.network(
//                 'https://source.unsplash.com/random?sig=$index',
//                 fit: BoxFit.cover),
//           ),
//         ),
//       );
// }


/*
{
                return Card(
                  margin: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(9)),
                  child: Container(
                    margin: EdgeInsets.all(8),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: Image.network(
                          'https://source.unsplash.com/random?sig=$index',
                          fit: BoxFit.cover),
                    ),
                  ),
                );
              },
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: (3 / 2),
              ),
            )

*/