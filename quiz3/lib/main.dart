// ignore_for_file: unused_import, use_key_in_widget_constructors

import 'package:flutter/material.dart';
import 'package:quiz3/screen/LoginScreen.dart';
import 'package:quiz3/screen/HomeScreen.dart';
import 'package:quiz3/screen/HomeGridView.dart';

import 'MainApp.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        //visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: '/',
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.
        '/': (context) => MainApp(),
        '/first': (context) => LoginScreen(),
        // When navigating to the "/second" route, build the SecondScreen widget.
        '/second': (context) => HomeScreen(),
      },

      //MainApp()
      // home: Telegram()  //tugas 12,
      // home: PostDataApi() //tugas 13 a,
      //tugas 14 ,
      // home: LoginScreen() //tugas 13 b,
    );
  }
}
