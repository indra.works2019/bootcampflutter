// ignore: file_names
// ignore_for_file: use_key_in_widget_constructors, deprecated_member_use, file_names

import 'package:flutter/material.dart';
import 'package:quiz3/screen/ChatScreen.dart';
import 'package:quiz3/screen/HomeScreen.dart';
import 'package:quiz3/screen/LoginScreen.dart';

class MainApp extends StatefulWidget {
  @override
  _MainAppState createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  int _selectedIndex = 0;
  final _layoutPage = [HomeScreen(), ChatScreen(), LoginScreen()];
  void _onTabItem(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _layoutPage.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.home), title: Text("Home")),
          BottomNavigationBarItem(icon: Icon(Icons.chat), title: Text("Chat")),
          BottomNavigationBarItem(
              icon: Icon(Icons.login), title: Text("Login")),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIndex,
        onTap: _onTabItem,
      ),
    );
  }
}
