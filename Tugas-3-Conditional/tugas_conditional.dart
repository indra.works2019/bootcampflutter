import 'dart:io';

//tugas 1:
// void main() {
//   var answer;
//   //bool jawab = true;
//   stdout.write('apakah akan install?');

//   answer = stdin.readLineSync();
//   (answer.toString() == 'y' || answer.toString() == 'Y')
//       ? print('Anda Install New Program')
//       : print('Anda tidak jadi install new Program');
// }

//tugas 2: If  Then Else
// void main() {
//   print("----------------------");
//   print('Please choice role before start game (example below) :');
//   print('name:"others_name" role:SuperHero');
//   print('name:Jane role:Penyihir');
//   print('name:Jenita role:Guard');
//   print('name:Junaedi role:WareWolf');
//   print("============================\n");

//   var name, role;

//   stdout.writeln('please input your name?');
//   name = stdin.readLineSync();
//   stdout.writeln('please input your role?');
//   role = stdin.readLineSync();

//   if (name.length < 1 && role.length < 1) {
//     print("semua Nama atau Role  harus diisi( Role Pilih salah satu diatas!!");
//   } else if (role.length < 1) {
//     print("Halo $name, Pilih peranmu untuk memulai game!");
//   } else if (role.toUpperCase() == "PENYIHIR") {
//     print(
//         "Selamat datang di Dunia Werewolf,  \n ,Halo Penyihir $name kamu dapat melihat siapa yang menjadi werewolf!");
//   } else if (role.toUpperCase() == "GUARD") {
//     print(
//         "Selamat datang di Dunia Werewolf, Jenita\n Halo Guard $name, kamu akan membantu melindungi temanmu dari serangan werewolf.");
//   } else if (role.toUpperCase() == "WAREWOLF") {
//     print("Selamat datang di Dunia Werewolf, Junaedi"
//         "Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!");
//   } else if (role.toUpperCase() != "WAREWOLF" ||
//       role.toUpperCase() != "GUARD" ||
//       role.toUpperCase() != "PENYIHIR") {
//     print(
//         "Selamat datang di Dunia Werewolf, \n,Halo Superhero $name, Kamu  hanya bisa menonton karena tidak pilih role! :D");
//   }
// }

//tugas 3. switch of :
// void main() {
//   var myCurrDt = DateTime.now();
//   switch (myCurrDt.weekday) {
//     case 1:
//       print("SENIN :\n\n");
//       print(
//           "Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.");
//       break;
//     case 2:
//       print("SELASA :\n\n");
//       print(
//           "Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.");
//       break;
//     case 3:
//       print("RABU :\n\n");
//       print(
//           "Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri");
//       break;
//     case 4:
//       print("KAMIS\n\n");
//       print(
//           "Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.");
//       break;
//     case 5:
//       print("JUMAT\n\n");
//       print("Hidup tak selamanya tentang pacar.");
//       break;
//     case 6:
//       print("SABTU\n\n");
//       print("Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.");
//       break;
//     case 7:
//       print("MINGGU\n\n");
//       print("Hanya seseorang yang takut yang bisa bertindak berani.");
//       print("Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.");
//       break;
//     default:
//       print("saat ini system of Quete this day:\n\n");
//       print("Sabar Dalam Ujian dari Allah SWT ");
//   }
// }

//tugas 4:witch case konevris bulan jadi string;

void main() {
  var hari = 21;
  var bulan = 12;
  var tahun = 1945;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945';

  switch (bulan) {
    case 1:
      print('$hari Januari $tahun ');
      break;
    case 2:
      print('$hari Februari $tahun ');
      break;
    case 3:
      print('$hari Maret $tahun ');
      break;
    case 4:
      print('$hari April $tahun ');
      break;
    case 5:
      print('$hari Mei $tahun ');
      break;
    case 6:
      print('$hari Juni $tahun ');
      break;
    case 7:
      print('$hari Juli $tahun ');
      break;
    case 8:
      print('$hari Agustus $tahun ');
      break;
    case 9:
      print('$hari September $tahun ');
      break;
    case 10:
      print('$hari October $tahun ');
      break;
    case 11:
      print('$hari Nopember $tahun ');
      break;
    case 12:
      print('$hari December $tahun ');
      break;

    default:
  }

  //mengganti bulan menjadi 'huruf
}

/*
CATATAN :
ternary state_condition?true:false;
if_else 
opsi1: if() {true}
opss2: if(){true} else {false};
opsi3 >2 kalang 
     if(){true} else if {true} else {false}
case switch:
    switch(variable_choice) {
  case 1:   { statement; break; }
  case 2:   { statement; break; }
  case 3:   { statement; break; }
  case 4:   { statement; break; }
  default:  { statement; }}
}

*/
