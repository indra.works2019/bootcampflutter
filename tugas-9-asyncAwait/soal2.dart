Future delayToPrint(int s, String msg) {
  return Future.delayed(Duration(seconds: s)).then((value) => msg);
}

void main() {
  delayToPrint(2, "Never Flat").then((checkTrue) {
    print(checkTrue);
  });
  print("Life");
  print("is");
}
