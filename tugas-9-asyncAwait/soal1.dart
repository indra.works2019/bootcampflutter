class Human {
  String name = "nama Char One Piece";

  void getData() {
    name = "hilmy";
    print('get data [done]');
  }

  Future<void> getName() async {
    await Future.delayed(Duration(seconds: 3),
        () => {name = "Hilmy", print("get data $name [done]")});
  }
}

void main() {
  Human h = Human();
  print(h.name);
  h.getName();
  print('Luffy');
  print('Zorro');
  print('Killer');
}

