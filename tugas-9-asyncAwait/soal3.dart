//<void> inilangsung dicetak !
Future<void> myCetakSing(int second, String msg) async {
  await Future.delayed(Duration(seconds: second), () => print(msg));
}

void main() {
  Map<int, String> cetak = {
    5: "Pernahkah Kau Merasa.",
    8: "Pernahkan Kau Merasa .......",
    10: "Pernahkan Kau Merasa....",
    11: "hatimu hampa pernahkah kau merasa hati mu kosong ............"
  };

  cetak.forEach((key, value) {
    myCetakSing(key, value);
  });
}
