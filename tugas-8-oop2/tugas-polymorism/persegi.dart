import 'bangun_datar.dart';

class Persegi extends Bangun_Datar {
  late double _sisi;

  //setter
  set sisi(double value) {
    if (value < 0) {
      value *= -1;
    }
    _sisi = value;
  }

  //getter
  double get sisi {
    return _sisi;
  }

  @override
  double luas() {
    //luas = sisi*sisi
    return _sisi * _sisi;
    throw UnimplementedError();
  }

  @override
  double keliling() {
    return 4 * _sisi * _sisi;
    //wajib diiisi diakhir baris method
    throw UnimplementedError();
  }
}
