import 'bangun_datar.dart';
import 'dart:math';

class Segitiga extends Bangun_Datar {
  //declare setter getter _alas
  late double _alas, _tinggi;
  void set alas(double value) {
    if (value < 0) {
      value *= -1;
    }
    _alas = value;
  }

  double get alas {
    return _alas;
  }

  //declare setter getter _tinggi
  void set tinggi(double value) {
    if (value < 0) {
      value *= -1;
    }
    _tinggi = value;
  }

  double get tinggi {
    return _tinggi;
  }

  @override
  double keliling() {
    double _sm;
    _sm = sqrt(pow(_alas, 2) + pow(_tinggi, 2));
    return _tinggi + _sm + _alas;

    // ignore: dead_code
    throw UnimplementedError();
  }

  @override
  double luas() {
    return 0.5 * _alas * _tinggi;

    // ignore: dead_code
    throw UnimplementedError();
  }
}
