import 'bangun_datar.dart';

class Lingkaran extends Bangun_Datar {
  final _pi = 3.14;
  late double _jari;

//setter
  set jari(double value) {
    if (value < 0) {
      value *= -1;
    }
    _jari = value;
  }

//getter
  double get jari {
    return _jari;
  }

  // note:otomatis terbuat overidenya dgn klik kanan sorot
  //jika tidak tulis manual @overridenya artinya
  //menggunakan template method Luas & keliling dari parentnya

  @override
  double keliling() {
    // TODO: implement keliling
    //rumus 2*pi*r
    return 2 * _pi * _jari;
    throw UnimplementedError();
  }

  @override
  double luas() {
    // TODO: implement luas
    //pi*r^2
    return _pi * _jari * _jari;
    throw UnimplementedError();
  }
}
