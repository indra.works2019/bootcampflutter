import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main() {
  Persegi pg = Persegi();
  Lingkaran lk = Lingkaran();
  Segitiga tg = Segitiga();

  //hitung luas,keliling  persegi;declare sisi
  pg.sisi = 31.5;
  pg.luas();
  pg.keliling();
  print(
      'persegi dgn sisi = ${pg.sisi} luas =${pg.luas()} dan keliling=${pg.keliling()}');
  //hitung luas,keliling egitiga ;declare alas,tinggi
  tg.alas = 71.6;
  tg.tinggi = 108.5;
  tg.keliling();
  tg.luas;
  print(
      'segitiga dgn alas = ${tg.alas}, tiggi =${tg.tinggi} luas =${tg.luas()} dan keliling=${tg.keliling()}');
  //hitung luas,keliling lingkara ;declare jari
  lk.jari = 9.721;
  lk.keliling();
  lk.luas;
  print(
      'lingakara dgn jari = ${lk.jari} luas =${lk.luas()} dan keliling=${lk.keliling()}');
}

/*
Soal 2 Polymorism
Buatlah file baru di dalam Tugas-8-oop2 dengan nama Tugas-Polymorism lalu di dalam folder tersebut beri lagi file class dengan nama bangun_datar, segitiga, lingkaran, persegi, dan juga file main.dart untuk menjalankan classnya

pada bangun_datar akan mereturn luas dan keliling,

pada class Lingkaran akan mengextend dari bangun_datar

setelah itu akan dimasukan object nya yaitu rumus keliling dan luas ,

class persegi juga sama, adanya object luas sisi * sisi dan keliling =  4 * sisi  ,

class segitiga ada luas = 0.5 * alas * tinggi, keliling = a +b + t, segitiga yang digunakan yaitu siku-siku

class lingkaran ada luas = 3.14 * jari-jari * jari-jari, keliling = 2 * pi * jari-jari 

dan main.dart akan memanggil object bangun_datar.dart, selain itu 
juga akan memanggil class segitiga, persegi dan lingkaran dan akan mencetak luas dan keliling;


*/



