class Titan {
  late int _powerPoint;

  //setter
  void set powerPoint(int value) {
    if (value < 5) {
      value = 5;
    }
    _powerPoint = value;
  }

  //getter
  int get powerPoint {
    return _powerPoint;
  }
  // no method

}
