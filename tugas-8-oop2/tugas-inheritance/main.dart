import 'lib/armor_titan.dart';
import 'lib/attack_titan.dart';
import 'lib/beast_titan.dart';
import 'lib/human.dart';
import 'lib/titan.dart';

void main() {
  Titan t = Titan();
  Armor_Titan arm_t = Armor_Titan();
  Attack_Titan atck_t = Attack_Titan();
  Beast_Titan bst_t = Beast_Titan();
  Human h = Human();

  List<int> nilai_power = [3, 1, 10, 13, 50];
  List<Titan> child = [t, arm_t, atck_t, bst_t, h];
  //isi pointPower
  for (var i = 0; i < child.length; i++) {
    child[i].powerPoint = nilai_power[i];
  }
  //tampilkan titan dan human dan power-nya
  for (var i = 0; i < child.length; i++) {
    print("${child[i]} have powerPoint : ${child[i].powerPoint}");
  }
}
