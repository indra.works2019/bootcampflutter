class Employee {
  //declare field/atribute
  late int _salary, _yrs_exp;
  late String _id, _name, _jabatan, _alamat;

  // Employee(String id, String name, String jabatan, int yrsExp, int salary,
  //     String alamat) {
  //   this._id = id;
  //   this._name = name;
  //   this._jabatan = jabatan;
  //   this._yrs_exp = yrsExp;
  //   this._salary = salary;
  //   this._alamat = alamat;
  // }
  //cara 2 declare constructor  parameter optional
  Employee(
      {required String id,
      required String name,
      required String jabatan,
      required int yrsExp,
      required int salary,
      required String alamat})
      : _id = id,
        _name = name,
        _jabatan = jabatan,
        _yrs_exp = yrsExp,
        _salary = salary,
        _alamat = alamat;

  //seter getter id
  set id(String id) {
    _id = id;
  }

  String get id {
    return _id;
  }

  //seter getter salary
  set salary(int value) {
    if (value < 0) {
      value = 100000;
    }
    _salary = value;
  }

  int get salary {
    return _salary;
  }

  //seter getter name
  set name(String value) {
    _name = value;
  }

  String get name {
    return _name;
  }

  //seter getter jabatan
  set jabatan(String value) {
    _jabatan = value;
  }

  String get jabatan {
    return _jabatan;
  }

  //seter getter alamat
  set alamat(String value) {
    _alamat = value;
  }

  String get alamat {
    return _alamat;
  }

  //years eperience

  set yrsExp(int value) {
    if (value < 0) {
      value = 1;
    }
    _yrs_exp = value;
  }

  int get yrsExp {
    return _yrs_exp;
  }

  String Output() => "${name} ${jabatan} ${salary}";
}


/*
PostList(
      //  @required Function() createPost ganti pakai this.
      {@required List<Post> posts,
      @required this.createPost})
      : _posts = posts;



*/