//SOAL 1

// class Segitiga {
//   //create property/field
//   final setengah = 0.5;
//   late double alas;
//   late double tinggi;

//   //create method return
//   double luasSegitiga() {
//     return setengah * alas * tinggi;
//   }
// }

// void main() {
//   //declare and instansiate object
//   Segitiga mysegitiga = Segitiga();
//   //kasih value ke property obj yg baru diinstansiate
//   mysegitiga.alas = 31.6;
//   mysegitiga.tinggi = 5.8;
//   //print hasil
//   print(mysegitiga.luasSegitiga());
// }

//SOAL 2.ENCAPSULATION
import 'Lingkaran.dart';

void main() {
  //declare & instansiate obj baru dari class linkaran
  //nama mylingkaran
  Lingkaran mylingkaran = Lingkaran();
  mylingkaran.alas = -0.6;
  mylingkaran.tinggi = 33.54;
  print("luas lingkaran = ${mylingkaran.luas}");
}
