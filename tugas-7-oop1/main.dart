//SegiPanjang dijadikan file terpisah namaClass sama dgn namafile;
//dan di import di main program

import 'SegiPanjang.dart';

void main() {
  //SegiPanjang persegi; //inisialisasi property objec segipanjang
  //bertipe data Class SegiPanjang
  double LuasSegiPanjang; //declare varibale utk tampung hasil luas
  //instansiate persegi atau mngcreate cetakan persegi yg akan berisi
  //peroperty2 dari class SegiPanjang yg akan dilekatkan pada object
  //cara paling lazim adalah dijadikan satu langusung
  //persegi = SegiPanjang();
  //cara cepat lazim langsung create object dan instansiate object sbb
  SegiPanjang persegi = SegiPanjang();
  persegi.lebar =
      3.72; //isi dgn set /access modifier ke local property class SegiPanjang
  persegi.panjang = 41.9;
  //isi degn set panjang property class yg udah melekat pada obj persegi
  //yg mana nilai diset dgn access ke local property _panjang pada  class SegiPanjang
  print(persegi.luas); //print hasil luas dengan memanggil property local luas
}
