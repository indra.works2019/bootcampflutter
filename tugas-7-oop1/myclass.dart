class PersegiPanjang {
  //declarasi atribut/variable bertipe double
  late double panjang;
  late double lebar;
  //method
  double hitungLuas() {
    return this.panjang * this.lebar; //method harus return
  }
}

//program utama
void main() {
  PersegiPanjang kotak; //declarasi object bertipe class PersegiPanjang
  double luasKotak;
  //instansiate object kotak dari class PersegiPanjang
  kotak = new PersegiPanjang();
  //mmberi nilai properti yg melekat pada object yg berasal dari class
  //dari class PersegiPanjang
  kotak.lebar = 2;
  kotak.panjang = 5;
  //gunakan methode PrsegiPanjang utk hitung object hasil ditampung
  //di variable luasKotak
  luasKotak = kotak.hitungLuas();
  print(luasKotak);
}
