class SegiPanjang {
  //_variable ini nunjukan local access tanda underscore "_"
  late double _panjang; // declare local _variable double tipe =double
  late double _lebar; // declare local _variable double

  //mmbuat methode setter  dan getter utk property local _lebar
  void set lebar(double value) {
    if (value < 0) {
      //check jika value parameter negatif
      value *= -1; //dijadikan positif selalu a= a*-1;
    }
    _lebar = value; //nilai value dimasukan ke _localVariable atau _lebar
  }

  // method getter local lebar atau _lebar
  double get lebar {
    return _lebar; //mngambil nilai _lebar yg udah di set di method setter _lebar
  }

  //mmbuat setter dan getter utk local variable panjang atau _panjang
  void set panjang(double value) {
    //check validation jadikan nilai positif
    if (value < 0) {
      value *= -1;
    }
    _panjang = value; //masukan nilai paremeter ke local variable atau _panjang
  }

  double get panjang {
    return _panjang; //ambil nilai local panjang
  }

  //ambil hasil dari method luas yg merupakan = _panjang * _lebar;
  double get luas => _panjang * _lebar;
}
