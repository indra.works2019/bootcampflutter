class Lingkaran {
  //declare local property
  final setengah = 0.5;
  late double _alas; //local
  late double _tinggi; //local

  //setter _alas
  set alas(double value) {
    if (value < 0) {
      value *= -1;
    }
    _alas = value;
  }

  //getter _alas
  double get alas {
    return _alas; //ambil/kembalikan _alas
  }

  //setter tinggi
  set tinggi(double value) {
    //check negatif
    if (value < 0) {
      value *= -1;
    }
    _tinggi = value;
  }

  //get local tinggi
  double get tinggi {
    return _tinggi;
  }

  //method local luas note ini bukan
  //bukan metode tapi property hasil dari
  //perkalian 2 buah property local tampak sprti metode

  double get luas => _tinggi * _alas * setengah;
}
