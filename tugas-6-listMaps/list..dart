//tugas 1:
// void main() {
//   List<int> Range(int start, int stop) {
//     List<int> mylist = [];
//     // print(mylist); //hasil []
//     //manual mmbuat link list

//     for (var i = start; i < (stop + 1); i++) {
//       mylist.add(i);
//     }
//     return mylist;
//   }

// //call Range :
//   print(Range(1, 10));
//   print(Range(11, 18));
// }

// //tugas 2

// void main() {
//   //list mirip array declarasinya
//   //list <type_data> nama_var = [isi_element_brdasar_typedata]

//   List<int> Range(int start, int stop, int step) {
//     List<int> mylist = [];
//     //print(mylist); //hasil []
//     //manual mmbuat link list

//     if (start > stop) {
//       for (var i = start; i > 1; i -= step) {
//         mylist.add(i);
//       }
//     } else {
//       for (var i = start; i < stop; i += step) {
//         mylist.add(i);
//       }
//     }

//     return mylist;
//   }

//   print(Range(1, 10, 1));
//   print(Range(20, 1, 2));
//   print(Range(5, 2, 1));
// }

//tugas 3:
//cara2 masing2 diubah dari list to tipe Map
//jangan pernah pakai addAll(pair) data selalu nimpa!!!!
void main() {
  //data yg akan di inputkan

  void DataHandling(List input) {
    List<Map> Result = [];
    List<String> listKey = ["ID", "Nama", "Alamat", "TLahir", "Hobby"];

    Map<String, String> data = {};

    for (var i = 0; i < input.length; i++) {
      data = Map.fromIterables(listKey, input[i]); //mmbuat pairnya
      data.addEntries(data.entries); //nambah pair
      Result.insert(i, data); //tambahkan di list
    }
    for (var i = 0; i < Result.length; i++) {
      print("${Result[i]}\n");
    }

    //clear memory
    Result.clear();
    data.clear();
  }

// MAIN PROGRAM
  var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Sanjaya", "Martapura", "12/03/1970", "Berkebun"],
  ];
  DataHandling(input);
}
// //tugas 4; ////
// void main() {
//   String BalikKata(String kata) {
//     var hasil = "";
//     //List<String> hasil = [];
//     for (var i = kata.length - 1; i >= 0; i--) {
//       //hasil.add(kata[i]); //masukan dri index trbesar
//       hasil += kata[i];
//     }
//     return hasil;
//   }

//   print(BalikKata("SanberCode"));
//   print(BalikKata("haji"));
//   print(BalikKata("RAceCar"));
//   print(BalikKata("TEtapSemangat Yang Penting BEnar!"));
// }





/*CATATATAN:

1)mmbuat list manual :
void main() {
  //list mirip array declarasinya
  //list <type_data> nama_var = [isi_element_brdasar_typedata]
  List<int> mylist = [];
  print(mylist); //hasil []
  //manual mmbuat link list

  for (var i = 1; i < 8; i++) {
    mylist.add(i);
  }
  print(mylist);
}
//note nilai i++ artinya = i=i+1;
//utk genap increment 2 bisa diganti dgn i+=2; mulau i =0;
//utk genap increment 2 bisa diganti dgn i+=3; mulau i =0;


2) mmbuat list comprehense :




3) note for multidimentional list:
1d : only col : List[0...n];
2d..3d....N : List[noIndexRow-keN][noIndexCol-keN];
https://medium.com/flutter-community/working-with-multi-dimensional-list-in-dart-78ff332430a
*/
