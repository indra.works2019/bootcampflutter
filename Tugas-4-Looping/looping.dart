//tugas 1 looping increment decrement
// void main() {
//   int i = 0;
//   print('LOOPING PERTAMA');
//   while (i < 21) {
//     i += 2;
//     print('$i - I love coding');
//   }
//   print('\n');
//   print('LOOPING KEDUA ');
//   while (i > 1) {
//     i -= 2;
//     print('$i - I will becone mobile developer');
//   }
// }

//tugas 2:loop for dgn syarat
/*
SYARAT:
A. Jika angka ganjil maka tampilkan Santai
B. Jika angka genap maka tampilkan Berkualitas
C. Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding.

*/
// void main() {
//   for (var i = 1; i < 21; i++) {

//     if (i % 2 == 1 && i % 3 == 0) {
//         print('$i I Love Coding');
//       } else if (i % 2 == 0) {
//         print('$i Berkualitas');
//       } else if (i % 2 == 1) {
//         print('$i Santai');
//       }

//   }
// }
//No. 3 Membuat Persegi Panjang #

// void main() {
//   var tmp = "#";
//   var result = "";
//   var i = 1;
//   while (i < 5) {
//     for (var j = 1; j <= 8; j++) {
//       result += tmp;
//       if (j == 8) {
//         print(result);
//       }
//     }
//     result = "";
//     i += 1;
//   }
// }

//no.4 No. 4 Membuat Tangga
void main() {
  var i = 1;
  var tmp = "#";
  var result = "";
  do {
    result += tmp;
    print(result);
    i += 1;
  } while (i < 8);
}
