//tugas 1
// void main() {
//   void teriak() => print("Hello Sanber");

//   teriak();
// }

//tugas 2

// void main() {
//   int kalikan(int num1, int num2) {
//     return num1 * num2;
//   }

//   var num1 = 12;
//   var num2 = 4;
//   var hasilkali = kalikan(num1, num2);
//   print(hasilkali);
// }

//tugas 3
//note pake ''' jika kepanjangan characternya
// void main() {
//   void introduce(name, age, address, hobby) => print(
//       "Nama saya $name, umur saya $age tahun, alamat saya di $address, dan saya punya hobby yaitu $hobby!");

//   var name = "Indra Suryawan";
//   var age = 33;
//   var address = "Jl. Gardenia Sawojajar Malang";
//   var hobby = "Martial Art,hiking,painting";

//   introduce(name, age, address, hobby);
// }

//tugas 4:factorial
//factorial(n)= n*(n-1)*...1;

import 'dart:io';
import 'dart:core';

void main() {
  int MyFactorial(int n) {
    var result = 1;
    if ((n == 0) || (n == 1)) {
      return result;
    } else {
      return result = n * MyFactorial(n - 1);
    }
  }

  int a, hasil;
  stdout.write('masukan bilangan bulat\n');
  a = int.parse(stdin.readLineSync() ?? '0'); //check null 
  hasil = MyFactorial(a);
  print('hasil faktorial dari ${a.toString()}! = ${hasil.toString()}');
}




/* catatan :2 jenis function 
function tanpa nilai balik : void nama_function() {} //1 baris void namaF()=>statetment;
function dgn nilai balik:type_data_return nama_functon(param) {retrun statement }



*/