import 'package:flutter/material.dart';
import 'route/route.dart' as route;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  //const MyApp({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FLutter Learn Routing',
      theme: ThemeData(primarySwatch: Colors.blueGrey),

      onGenerateRoute: route.controller, //aply ke file route
      initialRoute: route.registerPage, //initial route pada loginPage
    );
  }
}

//https://github.com/wobin1/flutter-navigation/blob/master/lib/main.dart
//https://medium.com/flutter-community/flutter-navigation-cheatsheet-a-guide-to-named-routing-dc642702b98c