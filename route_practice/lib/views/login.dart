// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:route_practice/route/route.dart' as route;

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold( 
      appBar: AppBar(title:Text('LoginPage')),
      body:Center(  child: ElevatedButton(
        onPressed: ()=>Navigator.pushNamed(context,route.homePage), 
        child: Text('Goto HomePage')),)
      
    );
  }
}
