// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors

import 'package:flutter/material.dart';
import 'package:route_practice/route/route.dart' as route;
// import nama_folder_file/dir/nama_file_dalam_dir.dart as route

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Page'),
      ),
      body: Center(
        child: ElevatedButton(
          child: Text("Go to Login page."),
          onPressed: () => Navigator.pushNamed(context, route.loginPage),
        ),
      ),
    );
  }
}
