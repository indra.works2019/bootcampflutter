// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:route_practice/route/route.dart' as route;

class RegisterPage extends StatelessWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Register Page'),
        ),
        body: Center(
          child: ElevatedButton(
              onPressed: () => Navigator.pushNamed(context, route.loginPage),
              child: Text('gotoLoginPage')),
        ));
  }
}
