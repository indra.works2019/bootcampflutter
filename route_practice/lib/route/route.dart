// ignore_for_file: prefer_equal_for_default_values, unused_import, prefer_const_constructors, duplicate_import

import 'package:flutter/material.dart';
import 'package:route_practice/views/home.dart';
import 'package:route_practice/views/login.dart';

import 'package:route_practice/views/register.dart';

//routes names
const String loginPage = 'login';
const String homePage = 'home';
const String registerPage = 'register';

login() {}
//controler funct to control page route flow
Route<dynamic> controller(RouteSettings settings) {
  switch (settings.name) {
    case loginPage:
      //manggil class HomePage
      return MaterialPageRoute(builder: (context) => LoginPage());
    case homePage:
      return MaterialPageRoute(builder: (context) => HomePage());

    case registerPage:
      return MaterialPageRoute(builder: (context) => RegisterPage());
    default:
      throw ('This route not found /doesnt exist!');
  }
}
